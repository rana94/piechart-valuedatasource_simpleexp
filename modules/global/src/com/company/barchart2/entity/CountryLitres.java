package com.company.barchart2.entity;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Column;
import com.haulmont.cuba.core.entity.StandardEntity;
import com.haulmont.chile.core.annotations.NamePattern;

@NamePattern("%s %s|country,litres")
@Table(name = "BARCHART2_COUNTRY_LITRES")
@Entity(name = "barchart2$CountryLitres")
public class CountryLitres extends StandardEntity {
    private static final long serialVersionUID = -4849107015427180744L;

    @Column(name = "COUNTRY")
    protected String country;

    @Column(name = "LITRES")
    protected Double litres;

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCountry() {
        return country;
    }

    public void setLitres(Double litres) {
        this.litres = litres;
    }

    public Double getLitres() {
        return litres;
    }


}